# -*- coding: utf-8 -*-
"""
Created on Mon Jun  6 16:16:10 2022

@author: Lenovo
"""

import rkhs_repr_norm
import rkhs_point_gen
import rkhs_fctgen
import rkhs_repr_norm
import numpy as np
from scipy import integrate

def fastNaiveOptimization(s, k, P, weights, iterations, kxk_int=0, kP_int=[], kPxkP=[]):
    l = [[-1,1]]
    n = len(P)
    
    def k_2(*args): 
        lists = [item for item in args]
        n = len(lists)
        x = lists[:n//2]
        y = lists[n//2:]
        return k(x,y)
    
    if (kxk_int == 0 or len(kP_int) == 0 or len(kPxkP) == 0):
        kxk_int = integrate.nquad(k_2, l * (2 * s))[0]
        kP_int = np.array([0.0] * n)
        kPxkP = np.array([[0.0] * n] * n)
        for i in range(n):
            def k_1(*x):
                return k(list(x),P[i])
            kP_int[i] = integrate.nquad(k_1, l * s)[0]
            
            for j in range(n):
                kPxkP[i][j] = k(P[i], P[j])
        
    for i in range(n):
        norm_old = rkhs_repr_norm.fastReprNorm(s, k, P, weights, kxk_int, kP_int, kPxkP)
        w_old = weights[i]
        w = None
        if i == 0: 
            w = weights[1:n]
        elif (i == n-1):
            w = weights[0:n-1]
        else:
            w = weights[0:i] + weights[i+1:n]
        
        min_w = min(w)
        wRange = [0, w_old + (n-1) * min_w]
        result = [None] * iterations
        h = wRange[1] / iterations
        l_min = 0
        for l in range(iterations):
            w_neu = l*h
            diff = w_old - w_neu

            weights_copy = [x + diff / (n-1) for x in weights]
            weights_copy[i] = w_neu
            
            norm_new = rkhs_repr_norm.fastReprNorm(s, k, P, weights_copy, kxk_int, kP_int, kPxkP)

            result[l] = norm_new
            
            if (result[l] < result[l_min]):
                l_min = l
        
        if result[l_min] < norm_old:
            diff = w_old - l_min*h
            weights = [x + diff / (n-1) for x in weights]
            weights[i] = l_min * h
            
    return weights

def naiveOptimization(s, k, P, weights, iterations):
    n = len(P)
    for i in range(n):
        norm_old = rkhs_repr_norm.reprNorm(s, k, P, weights)
        w_old = weights[i]
        w = None
        if i == 0: 
            w = weights[1:n]
        elif (i == n-1):
            w = weights[0:n-1]
        else:
            w = weights[0:i] + weights[i+1:n]
        
        min_w = min(w)
        wRange = [0, w_old + (n-1) * min_w]
        result = [None] * iterations
        h = wRange[1] / iterations
        l_min = 0
        for l in range(iterations):
            w_neu = l*h
            diff = w_old - w_neu

            weights_copy = [x + diff / (n-1) for x in weights]
            weights_copy[i] = w_neu
            
            norm_new = rkhs_repr_norm.reprNorm(s, k, P, weights_copy)

            result[l] = norm_new
            
            if (result[l] < result[l_min]):
                l_min = l
        
        if result[l_min] < norm_old:
            diff = w_old - l_min*h
            weights = [x + diff / (n-1) for x in weights]
            weights[i] = l_min * h
            
    return weights
        
        

def naiveOptimalLookup(s,k,P,n):
    if s == 1:
        if k["name"] == "polynomial":
            if k["c"] == 1.0:
                if k["d"] == 5:
                    if P == "equDistClosed": 
                        if n == 6:
                            return [0.0224274251677696, 1.1049874251677698, 0.2603947851677694, 0.22660788468776955, 0.22421442067496958, 0.16136805913395205]
                    if P == "equDistHalfClosed":
                        if n == 6:
                            return [0.1451638258958336, 0.0670838258958336, 0.3466665458958335, 0.37492384637583354, 0.44324071547535365, 0.622921240461312]
                    if P == "cheb1D":
                        if n == 6:
                            return [0.0003238973050879999, 0.8707238973050879, 0.6109478973050877, 0.19691218530508806, 0.160762956761088, 0.16032916601856004]
        if k["name"] == "gaussian":
            if k["gamma"] == 1:
                    if P == "equDistClosed": 
                        if n == 6:
                            return [0.15210277875548164, 0.38179877875548157, 0.4848613067554816, 0.43364532825948165, 0.2940871762149376, 0.253504631259136]
                    if P == "equDistHalfClosed":
                        if n == 6:
                            return [0.218030215380992, 0.350958215380992, 0.26487309538099196, 0.27227938018099196, 0.4266514892881919, 0.4672076043878399]
                    if P == "cheb1D":
                        if n == 6:
                            return [0.07813345878245376, 0.3610134587824537, 0.5878268987824536, 0.49699879510245376, 0.24658786672869376, 0.22943952182149116]
                        
        if k["name"] == "matern2":
            if k["m"] == 1:
                if k["h"] == 1.0:
                    if P == "equDistClosed": 
                        if n == 6:
                            return [0.16166906326810623, 0.39136506326810616, 0.46000429526810627, 0.43622219222810626, 0.3092855901775463, 0.24145379579002882]
                    if P == "equDistHalfClosed":
                        if n == 6:
                            return [0.1818958736546857, 0.37319187365468565, 0.27347782565468576, 0.2879466138786858, 0.41496813859215764, 0.46851967456509963]
                    if P == "cheb1D":
                        if n == 6:
                            return [0.11061726289919999, 0.34357726289920004, 0.5726908628991999, 0.5114082260991999, 0.2466172628992, 0.215089122304]
                        
                                 
