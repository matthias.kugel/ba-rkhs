# -*- coding: utf-8 -*-
"""
Created on Mon Nov 22 19:46:41 2021

@author: Lenovo
"""

import numpy as np
import matplotlib.pyplot as plt
import itertools
from sympy import Rational
import copy

"""
Klasse erzeugt von Cools/Poppe beschriebene Chebychev-Lattices.

"""

def getRandom(s, n):
    P = []
    for i in range(n):
        x = np.random.rand(s)
        x = [2*v - 1 for v in x]
        P.append(x)
    return P

def normalize(x):
    s = len(x)
    for i in range(s):
        x[i] = abs(x[i])
        x[i] = x[i] - (x[i] // 2) * 2
        if x[i] > 1: 
            x[i] = 2 - x[i]
    return x     

def getChebPoints(Z, d):
    s = len(Z[0]) 
    n = len(Z)
    
    zRanges = []
    erzX = []
    X = []
    
    for i in range(n-1):
        x = np.absolute(Z[i])
        x = x[x != 0]
        x = np.append(x, d[i])
        lcm = np.lcm.reduce(x)
        zRanges.append([-lcm, lcm])
        erzX.append(list(range(-lcm, lcm + 1)))
        
    W = []    
        
    for i in range(n):
        w = []
        for j in range(s):
            w.append(Rational(Z[i][j], d[i]))
        W.append(w)
    
    for x in itertools.product(*erzX):
        y = copy.copy(W[len(W)-1])
        for i in range(n-1):
            for j in range(s):
                w = W[i][j]
                y[j] = y[j] + x[i] * w
        y = normalize(y)
        if y not in X:
            X.append(y)
            
            
    for x in X:
        for i in range(len(x)):
            x[i] = np.cos(float(x[i]) * np.pi)
    return np.array(X)
               
def getEquDistClosedPoints(s, nDim):  
    P =[]
    for n in nDim:
        P.append(np.arange(n) / (n-1) * 2 - 1)

    Ps = []
    for x in itertools.product(*P):
        Ps.append(np.asarray(x))
        
    return np.array(Ps)

def getEquDistHalfClosedPoints(s, nDim):
    P =[]
    for n in nDim:
        P.append(np.arange(n) / (n) * 2 - 1)

    Ps = []
    for x in itertools.product(*P):
        Ps.append(np.asarray(x))
        
    return np.array(Ps)

def getCheb1DPoints(n):
    P = []
    for k in range(n):
        x = np.cos((2*(k+1) - 1) / (2*n) * np.pi)
        P.append(x)
        
    P = np.array(P)
    P = P.reshape(len(P),1)
    return P

def getChebnDPoints(nDim):
    P = []
    for n in nDim:
        P.append(getCheb1DPoints(n)[:,0])
        
    Ps = []
    for x in itertools.product(*P):
        Ps.append(np.asarray(x))
        
    return np.array(Ps)



