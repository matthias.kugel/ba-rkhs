# -*- coding: utf-8 -*-
"""
Created on Mon Oct  4 14:02:45 2021

@author: Lenovo
"""
import numpy as np
from scipy import special
import itertools
import rkhs_fctgen
from scipy import integrate
import math

"""
Integration von Funktionen, die als Linearkombination von Kerneln erzeugt 
wurden. Im Falle des Matern-Kernels mittels Scipys 'integrate.nquad'.

"""

######### Behelfsfunktionen für den Polynomkernel #######
def zerleg(d,level=0):
    d0 = d
    r = [[d0]]
    d = d0-1
    while (d >= 1):
        arr = zerleg(d0-d, level=level+1)
        arrNew = []
        for e in arr:
            if e[0] <= d:
                arrNew.append(([d] + e))
        r = r + arrNew
        d = d - 1
    return r

def berechneMultiindizes(d,s):
    multiindizes = []
    zerlegungen = zerleg(d)
    for zerlegung in zerlegungen:
        
        n = len(zerlegung)
        if (n <= s+1):
            S = list(range(1, s+2))
            permutationen = set(itertools.permutations(S,n))
            for permutation in permutationen: 
                multiindex = [0] * (s+1)
                for i in range(n):
                    multiindex[permutation[i]-1] = zerlegung[i]
                if (not multiindex in multiindizes):
                    multiindizes.append(multiindex)
    return multiindizes

def binomMulti(d,alpha):
    prod = math.factorial(d)
    for a in alpha:
        prod = prod / math.factorial(a)
    return prod

    

########## Linearer Kernel ########## 
def intLin(X,a):
    n = len(a)
    summe = 0
    
    for i in range(n):
        summe = summe + a[i] * sum(X[i])
        
    summe = summe / 2
    return 0.0

########## Polynomkernel ########## 
def intPoly(X, a, c, d):
    n = len(a)
    s = len(X[0])
    S = list(range(1, s + 1)) 
    gesamtSumme = 0 
    
    for i in range(n):
        x = X[i,:]
        x = x[x != 0]
        s = len(x)
        S = list(range(1,s+1))
        produkt = 1
        
        for j in range(s): 
            produkt = produkt / (x[j] * (d + j + 1))
            
        summe = 0
        
        for k in range(s + 1):  
            N = set(itertools.combinations(S,k))
            
            for tupel in N:
                teilSumme = 0
                
                for l in tupel: 
                    teilSumme = teilSumme + + x[l-1]
                    
                teilSumme = teilSumme + 1
                teilSumme = np.power(teilSumme, d + s)
                teilSumme = np.power(-1, s - k) * teilSumme
                summe = summe + teilSumme
                
        gesamtSumme = gesamtSumme + a[i] * produkt * summe
        
    return gesamtSumme
            

########## Polynomkernel 2########## 
def intPoly2(X, a, c, d):
    n = len(a)
    s = len(X[0])
    gesamtSumme = 0 
    multiindizes = berechneMultiindizes(d,s)
    
    for i in range(n):
        summe = 0
        for multiindex in multiindizes:
            prod = binomMulti(d,multiindex) * (c ** multiindex[s])
            for j in list(range(s)):
                prod = prod * (X[i][j] ** multiindex[j]) * (1 - ((-1) ** (multiindex[j] + 1))) / (multiindex[j] + 1)
            summe = summe + prod
        gesamtSumme = gesamtSumme + a[i] * summe
    return gesamtSumme

########## Gauß Kernel ########## 
def intGauss(X,a,sig):
    n = len(a)
    s = len(X[0])
    summe = 0
    
    for i in range(n):
        produkt = np.power(np.sqrt(np.pi) * sig / 2, s)
        
        for j in range(s):
            produkt = produkt * (special.erf((1-X[i][j])/sig) - special.erf((-1-X[i][j])/sig))
            
        produkt = a[i] * produkt
        summe = summe + produkt 
        
    return summe
            
########## Matern Kernel ##########
def intMatern(X,a,alpha,h):
    f = rkhs_fctgen.matern(X,a,alpha,h)
    
    def g(*args):
       lists = [item for item in args]
       return f(lists)
    
    s = len(X[0])
    l = [[-1,1]]
    
    return integrate.nquad(g, l * s)[0]

########## Matern Kernel ##########
def intMatern2(X,a,m,h):
    f = rkhs_fctgen.matern2(X,a,m,h)
    
    def g(*args):
       lists = [item for item in args]
       return f(lists)
    
    s = len(X[0])
    l = [[-1,1]]
    
    return integrate.nquad(g, l * s)[0]

########## Fast Lookup Function #######

def doubleIntLookup(k,s):
    if s == 1:
        if k["name"] == "linear":
            return None
        if k["name"] == "polynomial":
            if k["c"] == 1.0:
                if k["d"] == 7:
                    return None
                    
        if k["name"] == "gaussian":
            if k["gamma"] == 1:
                return None
   
        if k["name"] == "matern2":
            if k["m"] == 1:
                if k["h"] == 1.0:
                    return None
                
    if s == 2:
        if k["name"] == "linear":
            return 0.0
        if k["name"] == "polynomial":
            if k["c"] == 1.0:
                if k["d"] == 99:
                    return 5.571999662986145e+41
                elif k["d"] == 3:
                    return 26.666666666666664
                elif k["d"] == 10:
                    return 1476.4035809757022
                    
        if k["name"] == "gaussian":
            if k["gamma"] == 1:
                return 6.485381411410365
   
        if k["name"] == "matern2":
            if k["m"] == 1:
                if k["h"] == 1.0:
                    return 7.9973738373314625
                    
