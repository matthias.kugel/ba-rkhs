# -*- coding: utf-8 -*-
"""
Created on Tue Nov  2 15:03:11 2021

@author: Lenovo
"""

import rkhs_fctgen
import rkhs_kern_int
import rkhs_quad
import rkhs_fct_norm
from scipy import integrate
import numpy as np
import datetime 

"""
Die Klasse berechnet die Norm des Repräsentanten des Quadraturfehlers. 
Dies wird benötigt, um die RKHS-Quadratur-Fehlerabschätzung zu errechnen.
Die Norm hängt von der Punktemenge sowie der Quadratur ab. Folgende werden 
dabei berücksichtigt:

- "Standard"-Quadratur (Alle Gewichte 1/N)
- "Cools/Poppe"-Quadratur 
- Trapez (nur 1D)

"""
            
def reprNorm(s, k, P, weights): 
    l = [[-1,1]]

    def k_2(*args): 
        lists = [item for item in args]
        n = len(lists)
        x = lists[:n//2]
        y = lists[n//2:]
        return k(x,y)
    
    sum1 = integrate.nquad(k_2, l * (2 * s))
    sum1 = sum1[0]
    
    sum2 = 0
    sum3 = 0
    for i in range(len(P)):
        def k_1(*x):
            return k(list(x),P[i])
        
        sum2 = sum2 - 2*weights[i]*integrate.nquad(k_1, l * s)[0]
        for j in range(len(P)):
            sum3 = sum3 + weights[i]*weights[j]*k(P[i], P[j])
            
    return np.sqrt(sum1 + sum2 + sum3)

def fastReprNorm(s, k, P, weights, kxk_int, kP_int, kPxkP): 

    sum1 = kxk_int
    
    sum2 = 0
    sum3 = 0
    for i in range(len(P)):
        def k_1(*x):
            return k(list(x),P[i])
        
        sum2 = sum2 - 2*weights[i]*kP_int[i]
        for j in range(len(P)):
            sum3 = sum3 + weights[i]*weights[j]*kPxkP[i][j]

    return np.sqrt(sum1 + sum2 + sum3)
    

######## Standard Quadratur #################
#### Linear ####

def reprNormLin(s,P,quad):
    
    def k(*args): 
        lists = [item for item in args]
        n = len(lists)
        x = lists[:n//2]
        y = lists[n//2:]
        return rkhs_fctgen.k_lin(x,y)
    
    l = [[-1,1]]
            
    sum1 = integrate.nquad(k, l * (2 * s))
    sum1 = sum1[0]
    
    sum2 = 0 
    
    if quad == "std": 
        weights = rkhs_quad.stdWeights(P,s)
    else:
        if quad == "trapez":
            weights = rkhs_quad.trapezWeights(P,s)
        else:
            weights = rkhs_quad.coolsPoppeWeights(P,s)
    
    sum2 = sum2 + rkhs_kern_int.intLin(P,weights)
     
    sum2 = -2 * sum2 
    sum3 = (rkhs_fct_norm.normLin(P, weights)) ** 2

    return np.sqrt(sum1 + sum2 + sum3)
    
##### Poly ####
def reprNormPoly(s,P,c, d,quad):
    
    def k(*args): 
        lists = [item for item in args]
        n = len(lists)
        x = lists[:n//2]
        y = lists[n//2:]
        return rkhs_fctgen.k_poly(x,y,c,d)
    
    
    l = [[-1,1]]
            
    sum1 = integrate.nquad(k, l * (2 * s))
    sum1 = sum1[0]
    sum2 = 0 
    
    
    if quad == "std": 
        weights = rkhs_quad.stdWeights(P,s)
    else:
        if quad == "trapez":
            weights = rkhs_quad.trapezWeights(P,s)
        else:
            weights = rkhs_quad.coolsPoppeWeights(P,s)
    
    sum2 = sum2 + rkhs_kern_int.intPoly2(P,weights,c,d)
     
    sum2 = -2 * sum2 
    sum3 = (rkhs_fct_norm.normPoly(P, weights,c,d)) ** 2

    return np.sqrt(sum1 + sum2 + sum3)

##### Gauss ####
def reprNormGauss(s,P,sigma,quad):
    
    def k(*args): 
        lists = [item for item in args]
        n = len(lists)
        x = lists[:n//2]
        y = lists[n//2:]
        x = np.array(x)
        y = np.array(y)
        return rkhs_fctgen.k_gauss(x,y,sigma)
    
    l = [[-1,1]]
            
    sum1 = integrate.nquad(k, l * (2 * s))
    sum1 = sum1[0]
    sum2 = 0 
     
    if quad == "std": 
        weights = rkhs_quad.stdWeights(P,s)
    else:
        if quad == "trapez":
            weights = rkhs_quad.trapezWeights(P,s)
        else:
            weights = rkhs_quad.coolsPoppeWeights(P,s)
    
    sum2 = sum2 + rkhs_kern_int.intGauss(P,weights,sigma)
     
    sum2 = -2 * sum2 
    sum3 = (rkhs_fct_norm.normGauss(P, weights,sigma)) ** 2

    return np.sqrt(sum1 + sum2 + sum3)

##### Matern ####
def reprNormMatern(s,P,alpha,h,quad):
    def k(*args): 
        lists = [item for item in args]
        n = len(lists)
        x = lists[:n//2]
        y = lists[n//2:]
        x = np.array(x)
        y = np.array(y)
        return rkhs_fctgen.k_matern(x,y,alpha,h)
    
    l = [[-1,1]]
    sum1 = 0
    sum1 = integrate.nquad(k, l * (2 * s))
    sum1 = sum1[0]
    sum2 = 0 
    
    if quad == "std": 
        weights = rkhs_quad.stdWeights(P,s)
    else:
        if quad == "trapez":
            weights = rkhs_quad.trapezWeights(P,s)
        else:
            weights = rkhs_quad.coolsPoppeWeights(P,s)
    
    sum2 = sum2 + rkhs_kern_int.intMatern(P,weights,alpha,h)
     
    sum2 = -2 * sum2 

    sum3 = (rkhs_fct_norm.normMatern(P, weights,alpha,h)) ** 2

    return np.sqrt(sum1 + sum2 + sum3)

##### Matern ####
def reprNormMatern2(s,P,m,h,quad):

    def k(*args): 
        lists = [item for item in args]
        n = len(lists)
        x = lists[:n//2]
        y = lists[n//2:]
        x = np.array(x)
        y = np.array(y)
        return rkhs_fctgen.k_matern2(x,y,m,h)
    
    l = [[-1,1]]

    sum1 = integrate.nquad(k, l * (2 * s))
    sum1 = sum1[0]
    
    
    sum2 = 0 
    
    if quad == "std": 
        weights = rkhs_quad.stdWeights(P,s)
    else:
        if quad == "trapez":
            weights = rkhs_quad.trapezWeights(P,s)
        else:
            weights = rkhs_quad.coolsPoppeWeights(P,s)     


    sum2 = sum2 + rkhs_kern_int.intMatern2(P,weights,m,h)
     
    sum2 = -2 * sum2 
    
    
    sum3 = (rkhs_fct_norm.normMatern2(P, weights,m,h)) ** 2

    return np.sqrt(sum1 + sum2 + sum3)
    


