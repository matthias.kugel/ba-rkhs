# -*- coding: utf-8 -*-
"""
Created on Mon Oct  4 14:02:45 2021

@author: Lenovo
"""
import numpy as np
import scipy.special as sp
import math

"""
Kernelfunktionen, um Funktionen als Linearkombination von Kerneln mit
Stützpunkten (X) und Koeffizienten (a) zu erzeugen.

"""

########## Linearer  ########## 
def lin(X,a):
    def f(x): 
        m = len(a)
        f = 0
        for i in range(m):
            f = f + a[i] * np.dot(x, X[i,:])
        return f 
    return f

########## Poly ########## 
def poly(X, a, c, d):
    def f(x): 
        m = len(a)
        f = 0
        for i in range(m):
            f = f + a[i] * np.power((np.dot(x, X[i,:]) + c), d)
        return f 
    return f
                              
########## Gauß ########## 
def gauss(X,a,sig):
    def f(x): 
        m = len(a)
        f = 0
        for i in range(m):
            f = f + a[i] * np.exp(- np.power(np.linalg.norm(x - X[i,:]),2) / np.power(sig, 2))
        return f 
    return f
            
########## Matern ##########
def matern(X,a,alpha,h):
    def f(x):
        m = len(a)
        f = 0
        gamma = sp.gamma(alpha)
        for i in range(m):
            z = np.sqrt( 2 * alpha ) * np.linalg.norm(x - X[i,:]) / h
            f = f + a[i] * 1 / (np.power(2,alpha-1) * gamma) * np.power(z, alpha) * sp.kv(alpha, z)
        return f 
    return f

def matern2(X,a,m,h):
    def f(x):
        l = len(a)
        f = 0
        for i in range(l):
            f = f + a[i] * k_matern2(x, X[i],m,h)
        return f 
    return f

"""
Kernelfunktionen, die direkte Funktionsauswertung der Kernelfunktionen er-
moeglicht.

"""
########## Linearer Kernel ##########
def k_lin(x,y):
    return np.dot(x,y)

########## Poly Kernel ##########
def k_poly(x,y,c,d):
    return np.power((np.dot(x, y) + c), d)

########## Gauss Kernel ##########
def k_gauss(x,y,sig):
    x = np.array(x)
    y = np.array(y)
    return np.exp(- np.power(np.linalg.norm(x - y),2) / np.power(sig, 2))

########## Matern Kernel ##########                  
def k_matern(x,y,alpha,h):
    x = np.array(x)
    y = np.array(y)
    gamma = sp.gamma(alpha)
    z = np.sqrt( 2 * alpha ) * np.linalg.norm(x - y) / h
    return 1 / (np.power(2,alpha-1) * gamma) * np.power(z, alpha) * sp.kv(alpha, z)

########## Matern Kernel 2 (Sonderfall mit alpha = m + 1 / 2, wobei m natürlich) ##########
def k_matern2(x,y,m,h):
    x = np.array(x)
    y = np.array(y)
    e = np.exp( - np.sqrt(2 * (m + 1 / 2)) * np.linalg.norm(x - y) / h)
    gam = sp.gamma(m + 1) / sp.gamma(2 * m + 1)
    summe = 0
    for i in (range(m+1)): 
        summe = summe + (math.factorial(m+i) 
                     / math.factorial(i) 
                     / math.factorial(m-i) 
                     * (np.sqrt(8 * (m + 1/2)) 
                        * np.linalg.norm(x-y) / h) ** (m - (i)))
    return e * gam * summe
    
    