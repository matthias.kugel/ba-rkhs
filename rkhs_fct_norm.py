# -*- coding: utf-8 -*-
"""
Created on Mon Oct  4 14:02:45 2021

@author: Lenovo
"""
import numpy as np
import rkhs_fctgen

"""
Berechnung der Norm von Funktionen, die als Linearkombination von Kerneln 
erzeugt wurden. 

"""

########## Linearer Kernel ########## 
def normLin(X,a):
    k = rkhs_fctgen.k_lin
    summe = 0
    n = len(a)
    
    for i in range(n):
        
        for j in range(n):
            summe = summe + a[i] * a[j] * k(X[i], X[j])
    #print("lin norm squared = " + str(summe))
    return np.sqrt(summe)

########## Polynomkernel ########## 
def normPoly(X, a, c, d):
    k = rkhs_fctgen.k_poly
    summe = 0
    n = len(a)
    
    for i in range(n):
        
        for j in range(n):
            summe = summe + a[i] * a[j] * k(X[i], X[j], c, d)  
    return np.sqrt(summe)
            
########## Gauß Kernel ########## 
def normGauss(X,a,sig):
    k = rkhs_fctgen.k_gauss
    summe = 0
    n = len(a)
    
    for i in range(n):
        
        for j in range(n):
            summe = summe + a[i] * a[j] * k(X[i], X[j], sig)
    #print("gauss norm squared = " + str(summe))    
    return np.sqrt(summe)
         
########## Matern Kernel ##########
def normMatern(X,a,alpha,h):
    k = rkhs_fctgen.k_matern
    summe = 0
    n = len(a)
    
    for i in range(n):
        
        for j in range(n):
            summe = summe + a[i] * a[j] * k(X[i], X[j], alpha, h)
    #print("matern norm squared = " + str(summe))     
    return np.sqrt(summe)

########## Matern Kernel 2##########
def normMatern2(X,a,m,h):
    k = rkhs_fctgen.k_matern2
    summe = 0
    n = len(a)
    
    for i in range(n):
        
        for j in range(n):
            summe = summe + a[i] * a[j] * k(X[i], X[j], m, h)
    #print("matern2 norm squared = " + str(summe))       
    return np.sqrt(summe)
