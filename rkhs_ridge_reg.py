# -*- coding: utf-8 -*-
"""
Created on Mon Nov 22 11:43:58 2021

@author: Lenovo
"""
import rkhs_fctgen
import numpy as np

"""
Die Klasse für noise-free Kernel-Ridge-Regression mit linearen, polynomiellen,
Gauss- und Matern-Kerneln durch.

"""

def linReg(f, X):
    k = rkhs_fctgen.k_lin
    n = len(X)
    
    fX = np.zeros(n)
    
    for i in range(n):
        fX[i] = f(X[i])
    
    kXX = np.zeros((n,n))
    
    for i in range(n):
        for j in range(n):
            kXX[i,j] = k(X[i], X[j])

    det = np.linalg.det(kXX)
    if det == 0:
        res = np.zeros(n)
        res[:] = np.nan
        return [res]
            
    kXXInv = np.linalg.inv(kXX)
            
    return [kXXInv @ fX]

def polReg(f, X, c, d):
    k = rkhs_fctgen.k_poly
    n = len(X)
    
    fX = np.zeros(n)
    
    for i in range(n):
        fX[i] = f(X[i])
    
    kXX = np.zeros((n,n))
    
    for i in range(n):
        for j in range(n):
            kXX[i,j] = k(X[i], X[j], c, d)
            
    det = np.linalg.det(kXX)
    
    if det == 0:
        res = np.zeros(n)
        res[:] = np.nan
        return [res, c, d]
            
    kXXInv = np.linalg.inv(kXX)
            
    return [kXXInv @ fX, c, d]

def gaussReg(f, X, gamma):
    k = rkhs_fctgen.k_gauss
    n = len(X)
    
    fX = np.zeros(n)
    
    for i in range(n):
        fX[i] = f(X[i])
    
    kXX = np.zeros((n,n))
    
    for i in range(n):
        for j in range(n):
            kXX[i,j] = k(X[i], X[j], gamma)
            
            
    det = np.linalg.det(kXX)

    if det == 0:
        res = np.zeros(n)
        res[:] = np.nan
        return [res, gamma]
    
    kXXInv = np.linalg.inv(kXX)
    
            
    return [kXXInv @ fX, gamma]

def maternReg(f, X, alpha, h):
    k = rkhs_fctgen.k_matern
    n = len(X)
    
    fX = np.zeros(n)
    
    for i in range(n):
        fX[i] = f(X[i])
    
    kXX = np.zeros((n,n))
    
    for i in range(n):
        for j in range(n):
            kXX[i,j] = k(X[i], X[j], alpha, h)
            
    det = np.linalg.det(kXX)
    
    if (det == 0 or np.isnan(det)):
        res = np.zeros(n)
        res[:] = np.nan
        return [res, alpha, h]
            
    kXXInv = np.linalg.inv(kXX)
            
    return [kXXInv @ fX, alpha, h]

def matern2Reg(f, X, m, h):
    k = rkhs_fctgen.k_matern2
    n = len(X)
    
    fX = np.zeros(n)
    
    for i in range(n):
        fX[i] = f(X[i])
    
    kXX = np.zeros((n,n))
    
    
    
    for i in range(n):
        for j in range(n):
            kXX[i,j] = k(X[i], X[j], m, h)
            
    det = np.linalg.det(kXX)
    if (det == 0 or np.isnan(det)):
        res = np.zeros(n)
        res[:] = np.nan
        return [res, m, h]
            
    kXXInv = np.linalg.inv(kXX)
    
    return [kXXInv @ fX, m, h]
            