# -*- coding: utf-8 -*-
"""
Created on Wed Nov  3 14:44:33 2021

@author: Lenovo

"""
import numpy as np

"""
Die Klasse führt Quadratur auf einer Punktemenge P für eine Funktion f aus. 
Es stehen folgende Quadraturen zur Verfügung: 
    
- "Standard"-Quadratur (Alle Gewichte 1/N)
- Trapez-Regel (nur 1D)
- "Cools/Poppe"-Quadratur 
- 

"""

def manual(f,P,w):
    s = len(P[0])
    sum = 0
    N = len(P)
    for i in range(N):
        sum = sum + w[i]*f(P[i])
        
    return sum
    
def std(f,P): 
    s = len(P[0])
    sum = 0
    N = len(P)
    for x in P:
        sum = sum + f(x)
    return (2**s) * sum / N

def trapez(f,P):
    s = len(P[0])
    N = len(P)
    h = 1 / N
    P = P[1:N-1]
    sum = 0.5 * (f(np.array([-1])) + f(np.array([1]))) 
    for x in P: 
        sum = (2**s) * sum + f(x)
        
    return h * sum 

def coolsPoppe(f, P):
    s = len(P[0])
    sum = 0
    wSum = 0
    for x in P: 
        y = abs(np.array(x))
        wSum = wSum + (1/2 ** (len(y[y == 1])))
        sum = sum + (f(x) * (1/2 ** (len(y[y == 1]))))
    return (2**s) * sum / wSum
    
"""
Berechnung der Quadraturgewichte. Wird zur Berechnung der Norm des Reprä-
sentanten des Quadraturfehlers in rkhs_repr_norm.py verwendet.

"""

def stdWeights(P,s):
    return np.zeros(len(P)) + 1 * (2**s) / len(P)

def trapezWeights(P,s):
    n = len(P)
    h = 1 / n
    w = np.zeros(n)
    w[0] = w[n-1] = 1 / 2 * h
    w[1:n-1] = h
    return w

def coolsPoppeWeights(P,s):
    n = len(P)
    w = np.zeros(n)
    
    for i in range(n):
        y = abs(np.array(P[i]))
        w[i] = (1/2 ** len(y[y == 1]))
        
    w = w / sum(w)
    return w


        
        