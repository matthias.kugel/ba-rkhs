# -*- coding: utf-8 -*-
"""
Created on Fri Oct 22 12:41:30 2021

@author: Lenovo
"""

import rkhs_fctgen
import rkhs_weight_optimization 
import rkhs_point_gen
import rkhs_fct_norm
import rkhs_quad
import numpy as np
import rkhs_ridge_reg
import rkhs_kern_int
import rkhs_repr_norm
import matplotlib.pyplot as plt
from scipy import integrate
import csv
import xml.etree.ElementTree as et
import os
import glob
import logging
from random import random
import datetime
from scipy import special as sp

"""
(!) Change this Path to change Experiment
"""

expPath = 'experiments/example_1d/'


"""
Setting up the Experiment from setup.xml
"""
functions = []
quadP = []
interP = []
k = []
quad = []

tree = et.parse(expPath + 'setup.xml')
root = tree.getroot()

settings = root.find('settings')

# general settings 
s = int(settings.find('s').text)
savePlotImg = bool(settings.find('savePlotImg').text)
saveDataCSV = bool(settings.find('saveDataCSV').text)



"""
Other general settings
"""
# logging settings
with open(expPath + 'exp.log', 'w') as logfile:
    logfile.flush()
    logfile.close()


logging.basicConfig(filename = expPath + "exp.log", 
                    encoding="utf-8", 
                    level=logging.DEBUG)

logging.basicConfig(format='%(asctime)s %(message)s', 
                    datefmt='%m/%d/%Y %I:%M:%S %p')

# experiment counter
expNo = 1

# header for csv data-file
header = ["no.",
          "f", 
          "class of f",
          "kernel", 
          "interpolation pointset", 
          "interpolation coefficients",
          "quadrature pointset",
          "type of quadrature pointset",
          "no. of quadrature points",
          "quadrature methode",
          "||xi_K||_K",
          "||f*||_K", 
          "||xi_K||_K x ||f*||_K", 
          "2^s*max(|(err-0.5^s*Q(err; P))|)",
          "||xi_K||_K x ||f*||_K + 2^s*max(|(err-0.5^s*Q(err; P))|)", 
          "|I(f) - Q(f;P)|",
          "|I(f) - Q(f;P)| / |I(f)|"
          ]

# rows for csv data-file
data = []

#samplepoints for finding max of f on C^s
samplepoints = rkhs_point_gen.getEquDistClosedPoints(s, [100]*s)
plotSamplepoints = rkhs_point_gen.getEquDistClosedPoints(s, [100]*s)

l = [[-1,1]]

"""
getter functions
"""

# pointset

def getPointset(pointsetNode):
    type = pointsetNode.get('type')
    ps_name = pointsetNode.get("name")
    pointsets = []
    
    if (type == 'equDistClosed'):
        if ':' in pointsetNode.find('n').text:
            split = pointsetNode.find('n').text.split(':')
            m = int(split[0])
            n = int(split[1])
            for i in range(m,n+1):
                pointsets.append([rkhs_point_gen.getEquDistClosedPoints(s, [i]), type, ps_name])
            
        else:
            split = [eval(x) for x in pointsetNode.find('n').text.split('|')]
            if (len(split) == 1): 
                pointsets.append([rkhs_point_gen.getEquDistClosedPoints(s, split * s), type, ps_name]) 
            else:
                pointsets.append([rkhs_point_gen.getEquDistClosedPoints(s, split), type, ps_name])
            
            
    if (type == 'equDistHalfClosed'):
        if ':' in pointsetNode.find('n').text:
            split = pointsetNode.find('n').text.split(':')
            m = int(split[0])
            n = int(split[1])
            for i in range(m,n+1):
                pointsets.append([rkhs_point_gen.getEquDistHalfClosedPoints(s, 
                                                                           [i]), type, ps_name])
            
        else:
            split = [eval(x) for x in pointsetNode.find('n').text.split('|')]
            if (len(split) == 1): 
                pointsets.append([rkhs_point_gen.getEquDistHalfClosedPoints(s, split * s), type, ps_name])
            else:
                pointsets.append([rkhs_point_gen.getEquDistHalfClosedPoints(s, split), type, ps_name])
            
    if (type == 'cheb1D'):
        if ':' in pointsetNode.find('n').text:
            split = pointsetNode.find('n').text.split(':')
            m = int(split[0])
            n = int(split[1])
            for i in range(m,n+1):
                pointsets.append([rkhs_point_gen.getCheb1DPoints(i), type, ps_name])
            
        else:
            split = [eval(x) for x in pointsetNode.find('n').text.split('|')]
            pointsets.append([rkhs_point_gen.getChebnDPoints(split), type, ps_name])
            
    if (type == 'manual'):
        pointset = []
        for pointNode in pointsetNode:
            point = []
            for coordinateNode in pointNode:
                point.append(eval(coordinateNode.text))
            pointset.append(point)
            
        pointset = np.array(pointset)
        pointset = pointset.reshape(len(pointset), s)
        pointsets.append([pointset, type, ps_name])
        
        
    if (type == 'chebLattice'):
        denominators = []
        generatingVectors = []
        generatingVectorsNode = pointsetNode.find('generatingVectors')
        denominatorsNode = pointsetNode.find('denominators')
        
        for denominatorNode in denominatorsNode: 
            denominators.append(eval(denominatorNode.text))
        
        for generatingVectorNode in generatingVectorsNode:
            point = []
            for coordinateNode in generatingVectorNode:
                point.append(eval(coordinateNode.text))
            generatingVectors.append(point)
        
        pointsets.append([rkhs_point_gen.getChebPoints(generatingVectors, 
                                                      denominators), type, "chebLattice"])
    
    return pointsets

# kernels

def getKernel(kernelNode):
    type = kernelNode.get('type')
    if type == "linear":
        return {"name": "linear"}
    
    if type == "polynomial":
        c = float(kernelNode.find("c").text)
        d = int(kernelNode.find("d").text)
        return {"name": "polynomial", "c": c, "d": d}
    
    if type == "gaussian":
        gamma = float(kernelNode.find("gamma").text)
        return {"name": "gaussian", "gamma": gamma}
    
    if type == "matern1":
        alpha = float(kernelNode.find("alpha").text)
        h = float(kernelNode.find("h").text)
        return {"name": "matern1", "alpha": alpha, "h": h}
    
    if type == "matern2":
        m = int(kernelNode.find("m").text)
        h = float(kernelNode.find("h").text)
        return {"name": "matern2", "m": m, "h": h}
    
# functions 
def getFunction(functionNode):
    if (functionNode.get('type') == 'randomized'):
        
        iterations = int(functionNode.find("iterations").text)
        coeffInterval = eval(functionNode.find("coefficientRange").text)
        coeffMin = coeffInterval[0]
        coeffMax = coeffInterval[1]
        
        name = functionNode.find("name").text
        f_class = functionNode.find("class").text
        nameSplit = name.split('random')
        
        coeff = [None] * (len(nameSplit)-1)
        
        for iterationNo in range(iterations):
            for coeffNo in range(len(coeff)):
                coeff[coeffNo] = str(coeffMin + (random() * (coeffMax - coeffMin)))
            
            nameMerged = [None] * (len(nameSplit) + len(coeff))
            nameMerged[::2] = nameSplit
            nameMerged[1::2] = coeff
            name = ''.join(nameMerged)
            
            pythonDef = functionNode.find("pythonDef").text
            
            pythonDefSplit = pythonDef.split('random')
            
            pythonDefMerged = [None] * (len(pythonDefSplit) + len(coeff))
            pythonDefMerged[::2] = pythonDefSplit 
            pythonDefMerged[1::2] = coeff
            pythonDef = ''.join(pythonDefMerged)
            
            length = len(functions)
            pythonDefFct = "def f_" + str(length) + "(x):"
            pythonDefFct = pythonDefFct + """
            """
            pythonDefFct = pythonDefFct + "return " + pythonDef
            exec(pythonDefFct)
            functions.append({"name": name, "class": f_class, "f": eval("f_" + str(length))})
            
    else:
        f_class = functionNode.find("class").text;
        name = functionNode.find("name").text
        pythonDef = functionNode.find("pythonDef").text
        functions.append({"name": name, "class": f_class, "f": lambda x: eval(pythonDef)})
    
    
# getting stuff

quadraturePointsetsNode = root.find('quadraturePointsets')

for pointsetNode in quadraturePointsetsNode:
    quadP = quadP + getPointset(pointsetNode)

interpolationPointsetsNode = root.find('interpolationPointsets')

for pointsetNode in interpolationPointsetsNode:
    interP = interP + getPointset(pointsetNode)
    
    
kernelsNode = root.find('kernels')

for kernelNode in kernelsNode: 
    k.append(getKernel(kernelNode))
    
    
quadratureRulesNode = root.find('quadratureRules')

for quadratureRuleNode in quadratureRulesNode:
    
    quad.append(quadratureRuleNode.text)
    
    
functionsNode = root.find('functions')

for functionNode in functionsNode: 
   getFunction(functionNode)



logging.info("Setup done.")
print(str(datetime.datetime.now()) + ":    Setup done.")
if savePlotImg:
    files = glob.glob(expPath + "img/*")
    for file in files:
        os.remove(file)


for f in functions:

    logging.info("f(x)=" + str(f["name"]))
    
    def fWrapper(*x):
        return f["f"](np.asarray(list(x)))
    
    f_integral = integrate.nquad(fWrapper, l * s)[0]
    
    for kernel in k:


        logging.info("kernel=" + str(kernel["name"]))
        for interPointset in interP:
            logging.info("P_inter=" + str(interPointset[0]))
            
            """
            Interpolation durchführen
            
            """
            if (kernel["name"] == "linear"):
                def kern(x,y):
                    return rkhs_fctgen.k_lin(x,y)
                interpolationResult = rkhs_ridge_reg.linReg(f["f"], interPointset[0])
                
                if (not np.isnan(interpolationResult[0][0])):
                    fInterpol = rkhs_fctgen.lin(interPointset[0], 
                                                interpolationResult[0])
            
            if (kernel["name"] == "polynomial"):
                if kernel["d"] == -1:
                    d = len(interPointset[0]) - 1
                else:
                    d = kernel["d"]
                kernel["d"] = d
                def kern(x,y):
                    return rkhs_fctgen.k_poly(x, y, kernel["c"], d)
                interpolationResult = rkhs_ridge_reg.polReg(f["f"],
                                                            interPointset[0], 
                                                            kernel["c"], 
                                                            d
                                                            )
                if not np.isnan(interpolationResult[0][0]):
                    fInterpol = rkhs_fctgen.poly(interPointset[0], 
                                                 interpolationResult[0], 
                                                 kernel["c"], 
                                                 d
                                                 )
                
            if (kernel["name"] == "gaussian"):
                def kern(x,y):
                    return rkhs_fctgen.k_gauss(x, y, kernel["gamma"])
                interpolationResult = rkhs_ridge_reg.gaussReg(f["f"], 
                                                              interPointset[0], 
                                                              kernel["gamma"]
                                                              )
                if not np.isnan(interpolationResult[0][0]):
                    fInterpol = rkhs_fctgen.gauss(interPointset[0], 
                                                  interpolationResult[0],
                                                  kernel["gamma"]
                                                  )
                
            if (kernel["name"] == "matern"):
                def kern(x,y):
                    return rkhs_fctgen.k_matern(x, y, kernel["alpha"], kernel["h"])
                interpolationResult = rkhs_ridge_reg.maternReg(f["f"], 
                                                               interPointset[0], 
                                                               kernel["alpha"], 
                                                               kernel["h"]
                                                               )
                
                if not np.isnan(interpolationResult[0][0]):
                    fInterpol = rkhs_fctgen.matern(interPointset[0], 
                                                  interpolationResult[0],
                                                  kernel["alpha"],
                                                  kernel["h"]
                                                  )

            if (kernel["name"] == "matern2"): 
                def kern(x,y):
                    return rkhs_fctgen.k_matern2(x, y, int(kernel["m"]), float(kernel["h"]))
                interpolationResult = rkhs_ridge_reg.matern2Reg(f["f"], 
                                                                interPointset[0], 
                                                                kernel["m"], 
                                                                kernel["h"]
                                                                )
                
                if not np.isnan(interpolationResult[0][0]):
                    fInterpol = rkhs_fctgen.matern2(interPointset[0], 
                                                  interpolationResult[0],
                                                  kernel["m"],
                                                  kernel["h"]
                                                  )
                    
            """ save plot image """

            if not np.isnan(interpolationResult[0][0]):
                if savePlotImg:
                    if s == 1:
                        plot_fInterpol = []
                        plot_f = []
           
                        for i in range(len(plotSamplepoints)):
                            plot_fInterpol.append(fInterpol(plotSamplepoints[i]))
                            plot_f.append(f["f"](plotSamplepoints[i]))
                        
                                
                        filename = "exp_" + str(expNo) + "_plot"
                        
                            
                        plt.plot(plotSamplepoints, 
                                 plot_fInterpol, 
                                 "r", 
                                 plotSamplepoints, 
                                 plot_f, 
                                 "b"
                                 )
                        
                        plt.savefig(expPath + "img/" + filename + ".png")
                        plt.clf()
                    else:
                        if s == 2:  
                            filename = "exp_" + str(expNo) + "_plot"
                            
                            x = np.linspace(-1, 1, 30)
                            y = np.linspace(-1, 1, 30)
                            X, Y = np.meshgrid(x, y)
                            Z = fWrapper(X, Y)
                            
                            fig = plt.figure()
                            ax = plt.axes(projection='3d')
                            ax.contour3D(X, Y, Z, 50, cmap='binary')
                            ax.set_xlabel('x1')
                            ax.set_ylabel('x2')
                            ax.set_zlabel('f(x1,x2)')
                            
                            plt.savefig(expPath + "img/" + filename + ".png")
                            plt.clf()
            
                for quadPointset in quadP: 

                    """
                    BEGIN FAST STUFF PREPARATIONS
                    """

                    quadP_len = len(quadPointset[0])
                    def k_2(*args): 
                        lists = [item for item in args]
                        n_lists = len(lists)
                        x = lists[:n_lists//2]
                        y = lists[n_lists//2:]
                        return kern(x,y)

                    kxk_int = rkhs_kern_int.doubleIntLookup(kernel, s)
                    if kxk_int == None:
                        kxk_int = integrate.nquad(k_2, l * (2 * s))[0]
                    

                    kP_int = np.array([0.0] * quadP_len)
                    kPxkP = np.array([[0.0] * quadP_len] * quadP_len)
                    
                    
                    for i_fast in range(quadP_len):
                        def k_1(*x):
                            return kern(list(x),quadPointset[0][i_fast])
                        
                        if (kernel["name"] == "matern2"):
                            kP_int[i_fast] = integrate.nquad(k_1, l * s)[0]
                        elif (kernel["name"] == "matern"):
                            kP_int[i_fast] = integrate.nquad(k_1, l * s)[0]
                        elif (kernel["name"] == "gaussian"): 
                            kP_int[i_fast] = rkhs_kern_int.intGauss(np.array([quadPointset[0][i_fast]]), [1], kernel["gamma"]) 
                        elif (kernel["name"] == "polynomial"):
                            kP_int[i_fast] = rkhs_kern_int.intPoly(np.array([quadPointset[0][i_fast]]), [1], kernel["c"], d)
                        else:
                            kP_int[i_fast] = rkhs_kern_int.intLin(np.array([quadPointset[0][i_fast]]), [1])
                        
                        for j_fast in range(quadP_len):
                            kPxkP[i_fast][j_fast] = kern(quadPointset[0][i_fast], quadPointset[0][j_fast])
                            
                    
                    
                    """
                    END FAST STUFF PREPARATIONS
                    """
                    
                    if s == 2:
                        filename = "exp_" + str(expNo) + "_quadpoints_plot"
                        plt.scatter(quadPointset[0][:,0], quadPointset[0][:,1])
                        plt.savefig(expPath + "img/" + filename + ".png")
                        plt.clf()
                        
                        
                    logging.info("P_quad=" + str(quadPointset[0]))
                    for quadMethod in quad:

                        print(str(datetime.datetime.now()) + ":    Running experiment no. " + str(expNo) + " ...")
                        logging.info("method=" + str(quadMethod))
                        
                        
                        row = []
                        row.append(str(expNo))
                        row.append(str(f["name"]))
                        row.append(str(f["class"]))
                        row.append(str(kernel))
                        row.append(str(interPointset[0]))
                        row.append(str(interpolationResult[0]))
                        row.append(str(quadPointset[0]))
                        if quadPointset[2] is not None:
                            row.append(str(quadPointset[2]))
                        else:
                            row.append(str(quadPointset[1]))
                        row.append(str(len(quadPointset[0])))
                        row.append(quadMethod)
                        
                        # testet, ob die Interpolation erfolgreich war
                        
                        if not np.isnan(interpolationResult[0][0]):
                            is_manual = False
                            def err(x):              
                                return f["f"](x) - fInterpol(x)
                            
                            def errWrapper(*x): 
                                return err(np.asarray(list(x)))    
                            
                            #int_err = integrate.nquad(errWrapper, l * s)[0]
                            
                            
                            if (quadMethod == "std"):
                                quad_err = rkhs_quad.std(err, quadPointset[0])
                                weights = rkhs_quad.stdWeights(quadPointset[0], s)
                            elif (quadMethod == "coolsPoppe"):
                                quad_err = rkhs_quad.coolsPoppe(err, quadPointset[0])
                                weights = rkhs_quad.coolsPoppeWeights(quadPointset[0], s)
                            elif (quadMethod == "naiveOptimization"):
                                
                                weights = rkhs_quad.stdWeights(quadPointset[1], s)
                                weights = rkhs_weight_optimization.fastNaiveOptimization(s, 
                                                                                         kern, 
                                                                                         quadPointset[0], 
                                                                                         weights, 
                                                                                         100, 
                                                                                         kxk_int, 
                                                                                         kP_int, 
                                                                                         kPxkP)
                                
                                if weights == None:
                                    weights = [(2 ** s) / len(quadPointset[0])] * len(quadPointset[0])
                                    weights = rkhs_weight_optimization.fastNaiveOptimization(s, 
                                                                                             kern, 
                                                                                             quadPointset[0], 
                                                                                             weights, 
                                                                                             100, 
                                                                                             kxk_int, 
                                                                                             kP_int, 
                                                                                             kPxkP)
                                quad_err = rkhs_quad.manual(err, quadPointset[0], weights)
                                is_manual = True
                            else:
                                weights = eval(quadMethod)
                                is_manual = True
                                quad_err = rkhs_quad.manual(err, quadPointset[0], weights)
                            
                            def resid(x):
                                return err(x) - 0.5**s * quad_err
                            
                            
                                    
                            """
                            Maximum bestimmen
                            """
                            samples = []
                            for point in samplepoints:
                                samples.append(abs(resid(point)))
                                
                            res = 2**s * max(samples)
                            
                            
                            """Ab hier muss aufgeräumt werden!!!"""
                            if is_manual:
                                if kernel["name"] == "linear": 
                                    fctnorm = rkhs_fct_norm.normLin(interPointset[0], 
                                                                    interpolationResult[0])
                                    
                                elif kernel["name"] == "polynomial":

                                    
                                    fctnorm = rkhs_fct_norm.normPoly(interPointset[0], 
                                                                     interpolationResult[0], 
                                                                     kernel["c"], 
                                                                     d)
                                    
                                elif kernel["name"] == "gaussian":
                                    
                                    fctnorm = rkhs_fct_norm.normGauss(interPointset[0], 
                                                                      interpolationResult[0], 
                                                                      kernel["gamma"])

                                    
                                elif kernel["name"] == "matern":

                                    
                                    fctnorm = rkhs_fct_norm.normMatern(interPointset[0], 
                                                                       interpolationResult[0], 
                                                                       kernel["alpha"], 
                                                                       kernel["h"])
     
                                    
                                else:
                                    fctnorm = rkhs_fct_norm.normMatern2(interPointset[0], 
                                                                        interpolationResult[0], 
                                                                        kernel["m"], 
                                                                        kernel["h"])

                                repnorm = rkhs_repr_norm.fastReprNorm(s, 
                                                                      kern, 
                                                                      quadPointset[0],
                                                                      weights,
                                                                      kxk_int,
                                                                      kP_int,
                                                                      kPxkP)
                                
                                
                            else:
                                if kernel["name"] == "linear": 
                                    repnorm = rkhs_repr_norm.fastReprNorm(s, 
                                                                             kern,
                                                                             quadPointset[0],
                                                                             weights,
                                                                             kxk_int, 
                                                                             kP_int, 
                                                                             kPxkP)
                                    
                                    fctnorm = rkhs_fct_norm.normLin(interPointset[0], 
                                                                    interpolationResult[0])
                                    
                                if kernel["name"] == "polynomial":
                                    if (kernel["name"] == "polynomial"):
                                        if kernel["d"] == -1:
                                            d = len(interPointset[0]) - 1
                                    else:
                                        d = kernel["d"]
                                    kernel["d"] = d
                                    repnorm = rkhs_repr_norm.fastReprNorm(s, 
                                                                             kern,
                                                                             quadPointset[0],
                                                                             weights,
                                                                             kxk_int, 
                                                                             kP_int, 
                                                                             kPxkP)
                                    fctnorm = rkhs_fct_norm.normPoly(interPointset[0], 
                                                                     interpolationResult[0], 
                                                                     kernel["c"], 
                                                                     d)
                                    
                                if kernel["name"] == "gaussian":
                                    repnorm = rkhs_repr_norm.fastReprNorm(s, 
                                                                             kern,
                                                                             quadPointset[0],
                                                                             weights,
                                                                             kxk_int, 
                                                                             kP_int, 
                                                                             kPxkP)
                                    fctnorm = rkhs_fct_norm.normGauss(interPointset[0], 
                                                                      interpolationResult[0], 
                                                                      kernel["gamma"])
                                    
                                if kernel["name"] == "matern":
                                    repnorm = rkhs_repr_norm.fastReprNorm(s, 
                                                                             kern,
                                                                             quadPointset[0],
                                                                             weights,
                                                                             kxk_int, 
                                                                             kP_int, 
                                                                             kPxkP)
                                    fctnorm = rkhs_fct_norm.normMatern(interPointset[0], 
                                                                       interpolationResult[0], 
                                                                       kernel["alpha"], 
                                                                       kernel["h"])
                                    
                                if kernel["name"] == "matern2":
                                    repnorm = rkhs_repr_norm.fastReprNorm(s, 
                                                                             kern,
                                                                             quadPointset[0],
                                                                             weights,
                                                                             kxk_int, 
                                                                             kP_int, 
                                                                             kPxkP)
                                    fctnorm = rkhs_fct_norm.normMatern2(interPointset[0], 
                                                                        interpolationResult[0], 
                                                                        kernel["m"], 
                                                                        kernel["h"])

                            row.append(repnorm)
                            row.append(fctnorm)
                            row.append(repnorm * fctnorm)
                            row.append(res)
                            row.append(repnorm * fctnorm + res)
                            
                            if quadMethod == "std":
                                quadF = rkhs_quad.std(f["f"], quadPointset[0])
                            
                            elif quadMethod == "coolsPoppe":
                                quadF = rkhs_quad.coolsPoppe(f["f"], quadPointset[0])
                             
                            else:
                                quadF = rkhs_quad.manual(f["f"], quadPointset[0], weights)
                                

                            row.append(abs(quadF - f_integral))
                            if (abs(f_integral) != 0):
                                row.append(abs(quadF - f_integral) / abs(f_integral))
                            else:
                                row.append(np.nan)
                        else:
                            row.append(np.nan)
                            row.append(np.nan)
                            row.append(np.nan)
                            row.append(np.nan)
                            row.append(np.nan)
                            row.append(np.nan)
                            row.append(np.nan)

                        data.append(row)

                        print(str(datetime.datetime.now()) + ":    Experiment no. " + str(expNo) + " done.")
                        expNo = expNo + 1
                        
""" Ausgabe in CSV-Datei """
if saveDataCSV:     
    with open(expPath + "data.csv", 'w', newline='', encoding='UTF8') as c:
        writer = csv.writer(c)
        writer.writerow(header)
        writer.writerows(data)
 
















